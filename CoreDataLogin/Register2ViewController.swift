//
//  Register2ViewController.swift
//  CoreDataLogin
//
//  Created by Danae Panayiotidou on 28/09/2016.
//  Copyright © 2016 Velve7voice Ltd. All rights reserved.
//

import UIKit
import CoreData

class Register2ViewController: UIViewController {
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var newpassTextfield: UITextField!
    @IBAction func submitPressed(_ sender: AnyObject) {
        if newpassTextfield.text != ""{
            newPassword = newpassTextfield.text!
            saveToCoreData(name: newName, username: newUsername, password: newPassword)
            print("Name: \(newName), Username: \(newUsername), Password: \(newPassword)")
            performSegue(withIdentifier: "register3", sender: self)
        }else{
            errorLabel.isHidden = false
        }
    }
    
    var newName = ""
    var newUsername = "" 
    var newPassword = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        errorLabel.isHidden = true
        
        print("register2: \(newUsername)")

        // Do any additional setup after loading the view.
    }
    
    func saveToCoreData(name:String?, username:String, password: String){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let newUser = NSEntityDescription.insertNewObject(forEntityName: "Users", into: context)
        newUser.setValuesForKeys(["name": newName, "username": newUsername, "password": newPassword])
        
        do {
            try context.save()
            print("save successful")
            
        } catch {
            //
            print("error saving data")
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "register3"{
            let welcomeVC = segue.destination as! WelcomeViewController
            welcomeVC.name = newName
            welcomeVC.username = newUsername
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
