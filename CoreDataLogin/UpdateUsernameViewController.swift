//
//  UpdateUsernameViewController.swift
//  CoreDataLogin
//
//  Created by Danae Panayiotidou on 30/09/2016.
//  Copyright © 2016 Velve7voice Ltd. All rights reserved.
//

import UIKit
import CoreData

class UpdateUsernameViewController: UIViewController {
    
    var username = ""

    @IBOutlet var newUsernameTextfield: UITextField!
    
    @IBAction func saveButtonPressed(_ sender: AnyObject) {
        if (newUsernameTextfield.text != "") {
            let newUsername  = newUsernameTextfield.text!
            
            updateUsername(user: username, newUsername: newUsername)
            username = newUsername
            
            performSegue(withIdentifier: "saveupdate", sender: self)
        }
    }
    
    func updateUsername(user:String, newUsername: String){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        request.predicate = NSPredicate(format: "username =%@", user)
        request.returnsObjectsAsFaults = false
        
        do{
            let results = try context.fetch(request)
            
            if results.count > 0 {
                for result in results as! [NSManagedObject] {
                    
                    if let oldUser = result.value(forKey: "username") as? String{
                        result.setValue(newUsername, forKey: "username")
                        print ("\(oldUser) renamed to \(newUsername)")
                    } else {
                        print ("renaming failed")
                    }
                    
                    do {
                        try context.save()
                        print("new username: \(newUsername) saved successfully")
                    } catch {
                        
                        print("saving context with renamed username, failed")
                    }
                    
                }
            } else {
                print("username not found")
            }
        } catch {
            print("couldn't find user to update")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "saveupdate" {
        let welcomeVC = segue.destination as! WelcomeViewController
            welcomeVC.name = username
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
