//
//  RegisterViewController.swift
//  CoreDataLogin
//
//  Created by Danae Panayiotidou on 28/09/2016.
//  Copyright © 2016 Velve7voice Ltd. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var nameTextfield: UITextField!
    @IBOutlet var usernameTextfield: UITextField!
    
    @IBAction func submitPressed(_ sender: AnyObject) {
        
        if usernameTextfield.text != ""{
            
            if nameTextfield.text != ""{
                newName = nameTextfield.text!
            }
            newUsername = usernameTextfield.text!
            performSegue(withIdentifier: "register2", sender: self)
        } else {
            errorLabel.isHidden = false
        }
    }
    
    var newName = ""
    var newUsername = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        errorLabel.isHidden = true

        // Do any additional setup after loading the view.
        print("Registering new user: \(newUsername)")
        usernameTextfield.text = newUsername
        

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "register2"{
            
            let register2VC = segue.destination as! Register2ViewController
            register2VC.newUsername = newUsername
            register2VC.newName = newName
        
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
