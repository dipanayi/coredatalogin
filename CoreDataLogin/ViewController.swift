//
//  ViewController.swift
//  CoreDataLogin
//
//  Created by Danae Panayiotidou on 28/09/2016.
//  Copyright © 2016 Velve7voice Ltd. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    var welcomeName = "error"
    var newUserName = "error"
    var wrongPass = false

    @IBOutlet var usernameTextfield: UITextField!
    @IBOutlet var passwordTextfield: UITextField!
    @IBOutlet var errorLabel: UILabel!

    @IBAction func submitPressed(_ sender: AnyObject) {
        if usernameTextfield.text == "" && passwordTextfield.text == "" {
            
            errorLabel.text = "Please enter a username & password!"
            errorLabel.isHidden = false
        } else if usernameTextfield.text == "" {
            
            errorLabel.text = "Please enter a username!"
            errorLabel.isHidden = false
        } else if passwordTextfield.text == "" {
            
            errorLabel.text = "Please enter a password!"
            errorLabel.isHidden = false
        } else {
            let uname = usernameTextfield.text!
            let pass = passwordTextfield.text!
            
            let found = requestData(username: uname, password: pass)
            
            if found.count > 0 {
                let name = found[2]
                
                //go to segue welcome!
                welcomeName = name
                
                self.performSegue(withIdentifier: "welcome", sender: self)
                print("seque welcome \(welcomeName)")
                
                
            } else if wrongPass{
                //do nothing. user will have to enter correct password and press button again
                
            }else{
                //go to segue register
                newUserName = uname
                
                self.performSegue(withIdentifier: "register", sender: self)
                
                print("segue register \(newUserName)")
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "welcome"{
            
            let welcomeViewController = segue.destination as! WelcomeViewController
            welcomeViewController.name = welcomeName
            welcomeViewController.username = usernameTextfield.text!
        }
        
        if segue.identifier == "register"{
            
            let registerVC = segue.destination as! RegisterViewController
            registerVC.newUsername = newUserName
        
        }
    }
    
    
    
    
    func requestData(username: String, password: String) -> [String]{
        
        var found = [String]()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        request.returnsObjectsAsFaults = false
        
        do{
            let results = try context.fetch(request)
            if results.count > 0{
                
                for result in results as! [NSManagedObject]{
                    
                    if let user = result.value(forKey: "username") as? String{
                        
                        let uname = user
                        
                        if let passwd = result.value(forKey: "password") as? String{
                        
                            let pass = passwd
                            
                            if uname == username && pass == password {
                                
                                wrongPass = false
                                found = [uname, pass]
                                
                                if let realName = result.value(forKey: "name") as? String{
                                    let name = realName
                                    found.append(name)
                                    
                                } else{
                                    print("couldn't fetch name")
                                }
                                
                                break
                            } else if uname == username {
                                
                                errorLabel.text = "Incorrect password"
                                errorLabel.isHidden = false
                                wrongPass = true
                                
                            }
                            
                        } else{
                            print("couldn't fetch password")
                        }
                    } else {
                        print("couldn't fetch username")
                    }
                    
                }
            } else {
                
                print("No results")
                
            }
            
        } catch{
            
            print("Can't fetch results")
        }

    return found
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        errorLabel.isHidden = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

