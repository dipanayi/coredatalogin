//
//  WelcomeViewController.swift
//  CoreDataLogin
//
//  Created by Danae Panayiotidou on 28/09/2016.
//  Copyright © 2016 Velve7voice Ltd. All rights reserved.
//

import UIKit
import CoreData

class WelcomeViewController: UIViewController {

    @IBOutlet var helloLabel: UILabel!
    @IBAction func logOutButtonPressed(_ sender: AnyObject) {
        performSegue(withIdentifier: "logout", sender: self)
    }
    
    @IBAction func updateButtonPressed(_ sender: AnyObject) {
        performSegue(withIdentifier: "updateuser", sender: self)
        
    }
    
    @IBAction func deleteButtonPressed(_ sender: AnyObject) {
        print("deleting user... \(username)")
        deleteUser(user: username)
        performSegue(withIdentifier: "deleteuser", sender: self)
    }
    
    
    var name = ""
    var username = ""
    
    func deleteUser(user: String){
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appdelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        request.predicate = NSPredicate(format: "username =%@", user)
        request.returnsObjectsAsFaults = false

        do {
            let results = try context.fetch(request)
            
            if results.count > 0 {
                for result in results as! [NSManagedObject]{
                    
                    if let userToDelete = result.value(forKey: "username"){
                        context.delete(result)
                        
                        do{
                            try context.save()
                            print("\(userToDelete) deleted successfully")
                            
                        } catch {
                            
                            print("Deleting user \(userToDelete) failed")
                        }
                        
                    }else{
                        print("Couldn't get user")
                    }
                }
            } else {
                print("No results")
            }
        } catch {
            print("couldn't get results")
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Name: \(name)")
        if name != ""{
            helloLabel.text = "Hello, \(name)"
        } else {
            helloLabel.text = "Hello stranger!"
        }
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "updateuser"{
            let updateUsernameVC = segue.destination as! UpdateUsernameViewController
            updateUsernameVC.username = username
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
